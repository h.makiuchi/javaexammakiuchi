package turn2;

public class CastSample {

	public static void main(String[] args) {
		//右辺を明示的にfloat扱いにする
		float result = 10.1f / 3.1f; //エラーになる(1)
	    System.out.println(result);

	    int i = 32768;  //shortにギリギリ収まらない範囲の値

	    byte b = (byte)i;   // エラーになる(2)
	    System.out.println(b);
	    //shortの範囲を超えてしまったため
	    short s = (short)i;  //shortにキャスト
	    System.out.println(s);  //32768が表示されない(3)

	    double d = 12.345;
	    i = (int)d;        //intにキャスト

	    //intにしたので切り捨てられた
	    System.out.println(i); //12.345が表示されない(4)


	}

}
