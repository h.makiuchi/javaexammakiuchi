package turn2;

public class TryExpressionValue {

	public static void main(String[] args) {
		int a=3,b=5,c=8;
		boolean d=false,e=true;
		System.out.println("a+b："+(a+b));
		System.out.println("d | e："+(d | e));
		System.out.println("a==b："+(a==b));
		System.out.println("(a > 2)&&(b <10)："+((a > 2)&&(b <10)));
		System.out.println("(a < 2)||(b < 10)："+((a < 2)||(b < 10)));
		System.out.println("++a："+(++a));
		System.out.println("a=10："+(a=10));


	}

}
